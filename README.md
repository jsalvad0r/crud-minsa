Ejecutar

 -  ./manage.py makemigrations
 -  ./manage.py migrate
 - ./manage.py loaddata main/fixtures/data_init.json
 

Opcional

para generar data de prueba use el package faker



```
from main import models
from faker import Faker
fake = Faker()
for i in range(100):
	random_id = fake.random_int(1,3)
	if random_id == 3:
		document = None
	else:
		document = fake.random_int(10000000, 99999999)
	models.Paciente.objects.create(
		tipo_documento=models.TipoDocumento.objects.filter(id=random_id).first(),
		documento=document,
		nombre=fake.name(),
		apellido_paterno=fake.first_name(),
		apellido_materno=fake.last_name(),
		fecha_nacimiento=fake.date_of_birth()
	)
```

