from django.db import models
from django.urls import reverse
import datetime

class TipoDocumento(models.Model):
	tipo = models.CharField(max_length=30)

	def __str__(self):
		return "%s" % self.tipo


class Paciente(models.Model):
	tipo_documento = models.ForeignKey(TipoDocumento, on_delete=models.DO_NOTHING)
	documento = models.CharField(max_length=8, null=True)
	nombre = models.CharField(max_length=50)
	apellido_paterno = models.CharField(max_length=50)
	apellido_materno = models.CharField(max_length=50)
	fecha_nacimiento = models.DateField()

	class Meta:
		unique_together = ('tipo_documento', 'documento')

	def get_absolute_url(self):
		return reverse("main:detalle-paciente", kwargs={"id": self.id})
	
	@property
	def nombre_completo(self):
		return "%s %s %s" % (self.nombre, self.apellido_paterno, self.apellido_materno)

	@property
	def edad(self):
		fecha_nacimiento = self.fecha_nacimiento
		fecha_actual = datetime.date.today()
		
		prox_cumpleaños = datetime.date(fecha_actual.year, fecha_nacimiento.month, fecha_nacimiento.day)
		
		numero_años_cont = fecha_actual.year - fecha_nacimiento.year
		numero_dias_cont = fecha_actual.day
		
		if(prox_cumpleaños.month > fecha_actual.month):
			numero_años_cont -=1
			numero_mes_cont = fecha_actual.month + (11 - fecha_nacimiento.month)
		else:
			numero_mes_cont = (fecha_actual.month - prox_cumpleaños.month) - 1

		if(prox_cumpleaños.month == fecha_actual.month):
			numero_mes_cont = 0
			if(prox_cumpleaños.day > fecha_actual.day):
				numero_años_cont +=1
				numero_mes_cont -=1
				numero_dias_cont = prox_cumpleaños.day - fecha_actual.day
			else:
				numero_dias_cont = fecha_actual.day - prox_cumpleaños.day


		return "%s años, %s meses, %s dias" % (numero_años_cont, numero_mes_cont, numero_dias_cont)

