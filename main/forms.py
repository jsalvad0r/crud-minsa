from django import forms
from .models import Paciente, TipoDocumento

class PacienteForm(forms.ModelForm):
	tipo_documento = forms.ModelChoiceField(
		queryset=TipoDocumento.objects.all(),
		label="Tipo de documento",
		widget=forms.Select(
			attrs={
				"class": "form-control"
			}
		),
	)
	documento = forms.CharField(widget=forms.TextInput(
		attrs={"class": "form-control"}
	))
	nombre = forms.CharField(widget=forms.TextInput(
		attrs={"class": "form-control"}
	))
	apellido_paterno = forms.CharField(
		widget=forms.TextInput(
			attrs={"class": "form-control"}
		)
	)
	apellido_materno = forms.CharField(
		widget=forms.TextInput(
			attrs={"class": "form-control"}
		)
	)
	fecha_nacimiento = forms.CharField(widget=forms.TextInput(
		attrs={"class": "form-control", "type": "date"}
	))

	edad = forms.IntegerField(required=False, widget=forms.TextInput(
		attrs={'readonly': 'readonly'}
	))


	class Meta:
		model = Paciente
		fields = [
			'tipo_documento',
			'documento',
			'nombre',
			'apellido_paterno',
			'apellido_materno',
			'fecha_nacimiento',
			'edad'
		]