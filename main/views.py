from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from main import models, forms
from django.views.generic import (
	CreateView,
	UpdateView,
	ListView,
	DetailView,
	DeleteView
)

class CrearPacienteView(CreateView):
	template_name = "crear_paciente.html"
	form_class = forms.PacienteForm
	queryset = models.Paciente.objects.all()

	def form_valid(self, form):
		return super().form_valid(form)

	def get_success_url(self):
		return reverse("main:listar-paciente")

class DetallePacienteView(DetailView):
	template_name = "detalle_paciente.html"

	def get_object(self):
		id_ = self.kwargs.get("id")
		return get_object_or_404(models.Paciente, id=id_)

class ActualizarPacienteView(UpdateView):
	template_name = "actualizar_paciente.html"
	form_class = forms.PacienteForm

	def form_valid(self, form):
		return super().form_valid(form)

	def get_object(self):
		id_ = self.kwargs.get("id")
		return get_object_or_404(models.Paciente, id=id_)

	def get_success_url(self):
		return reverse("main:listar-paciente")

class EliminarPacienteView(DeleteView):
	def get_object(self):
		id_ = self.kwargs.get("id")
		return get_object_or_404(models.Paciente, id=id_)

	def get_success_url(self):
		return reverse("main:listar-paciente")

class ListarPacienteView(ListView):
	template_name = "listar_paciente.html"
	queryset = models.Paciente.objects.all()

	def get_context_data(self, **kwargs):
		context_ = super(ListarPacienteView, self).get_context_data(**kwargs)
		context_["tipos_documento"] = models.TipoDocumento.objects.all()
		return context_