from django.urls import path
from main import views

app_name = 'main'

urlpatterns = [
	path('', views.ListarPacienteView.as_view(), name='listar-paciente'),
	path('crear/', views.CrearPacienteView.as_view(), name='crear-paciente'),
	path('<int:id>/', views.DetallePacienteView.as_view(), name='detalle-paciente'),
	path('<int:id>/actualizar/', views.ActualizarPacienteView.as_view(), name='actualizar-paciente'),
	path('<int:id>/eliminar/', views.EliminarPacienteView.as_view(), name='eliminar-paciente')
]